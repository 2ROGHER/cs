﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");


// Variables y tipos de datos
int i = 0;
string str = "Hello world!";
long l = 1000000;
float f = 2.44f; // nota: se pone el (f) para que se tome en cuenta que es un float.
double d = 3.2523582935823d; // Nota: este tambien usa un (d) para que indique que es un numero de doble presicion
char c = 'a';

// Show variables in the console
Console.WriteLine("Int: " + i);
Console.WriteLine("String: " + str);
Console.WriteLine("Float: " + f);
Console.WriteLine("Double:" + d);

// Input and Output in C#
Console.WriteLine("Input  your age: ");
int age = Convert.ToInt32(Console.ReadLine());

Console.WriteLine("Input your name: ");
string name = Console.ReadLine();
Console.WriteLine("Hello, " + name + "welcome here my post");


// type conversions
// Implicit (no manual) min -> max
float miFloat = i;
Console.WriteLine("My new float: " + miFloat);
double miDouble = f; // here double is biger than float (esot quiere decir que absorve)
Console.WriteLine(miDouble);
string str2 = "24";
int miInt = Convert.ToInt32(str2);
Console.WriteLine("Este es mi int convertido con Convert.ToInt(str2)");
Console.WriteLine(miInt + 1);

// Constante es C#
const double PI = 3.1415;
const double r = 2;
double area = Math.Pow(r, 2) * PI;

// Nullables
// Esto se puede usar para i.e. cuando se quiere agregar un registro en la base de datos, pero el usuario
// no ingresa ningun valor en el campo.
// No  es lo mismo que "vacio" y nulo, el vacio quiere decir que si existe un valor, mientras que null, aucencia de valor
int? a = null; // esto nos quiere que decir que existe la variable pero no tiene valor.

Console.WriteLine(a + 1); // (linea vacia)
// tenemos que entender en estos casos que tipos de datos diferentes tienen presedencia de operadores.
Console.WriteLine(a.HasValue);// nos permite ver si la variable tiene un valor dentro de este.
string? strNull = null;


// OPERADORES EN C#
// Operadores Aritmeticos (* / + - % pow)
// Operadores de Comparacion ( === == < > <= >= )
// Operadores de Logicos (&& || !)
// 

