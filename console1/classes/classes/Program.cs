﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.IEnumerable;

// ************* QUE ES LINQ (Lenguaje integrado de consultas) - IMPORTANTE EN C# ***************
// Este es un lenguaje integrado dentro de .NET con los que podemos realizar consultas dentro de C#.
// Por medio de estas consultas podemos comunicarnos con basesdedatos (DB) y obtener el origen de los datos: file, DDBB, etc.
// Despues podemos crear la consulta con el lenguaje de C#.
// Luego ejecutar la consulta creada anteriomente.
// Gracias a estas consultas podemos obtener los datos para nuestro app con la ayuda de C# pero usando LINQ.
// No es primordial el uso de Queries fijos para DBs para obtener los datos en una DB.


// 1. LINQ

// 2. Nuestro Origen de datos
int[] nums = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
string[] people = new string[4] { "roger", "fiorela", "melissa", "vanessa" };
// 3. Creacion del Query con una consulta
var query = from n in nums
            where (n % 2) == 0
            select n;

var greatherThan5 = from n in nums
                   where n > 5
                   select n;
var impair = from n in nums
             where (n % 2) != 2
             select n;

var contain = from item in people
            where item.Contains("iorela")
            select item;

var lengthName = from item in people
                 where item.Length > 3
                 select item;

// Tipos de datos (Anonimo)
// Este se puede usar para c uando queremos crear objetos, cuando no tenemos una clase definida pero queremos
// usarlo como objetos.

var coche = new { Marca = "Ford", Year = 2032 }; // este es un tipo de dato anonimo
Console.WriteLine($"Tengo un coche marca : {coche.Marca}");
// metodo que trabaja con el tipo de dato anonimo
void ChangeName(string newName)
{
    // invest how we can change it 
    var myVar = coche.Marca;
    coche.Marca = newName;
}

main();
void main()
{
    Person p = new Person("Roger", 25, "brow", Convert.ToDateTime("04-23-1999"));
    Console.WriteLine(p.Name);

    Console.WriteLine(Person.sex); // Here we don't need to create a object to acces this static atribute
    Person.SayHello(); // here another public static method for Person class.

    // LINQs en C#
    Console.WriteLine(query);
    Console.WriteLine(greatherThan5);
}
// #################### Funciones EN C# #########################

float f = 3.14151692f;

WriteRealNumber(f);
SumRealNumbers(3.2f, 9.342f);
void WriteRealNumber(float f)
{
    Console.WriteLine(f.ToString("#.##")); // with 2 decimal points;
}

void SumRealNumbers(float a, float b)
{
    Console.WriteLine((a + b).ToString("#.#")); // with 1 decimal point;
}

// ########### RECURSIVIDAD EN C# #######################
// Esta es una forma de resolver un problema partinedo de la auto invocacion de la funcion definida misma.

// 
Console.WriteLine($"Factorial of a {5} is: {Factorial(5)}");
int Factorial(int n)
{
    if(n == 1)
    {
        return 1;
    }

    return n * Factorial(n - 1);
}

// ########### PROGRAMACION ORIENTADA A OBJETOS  EN C# #######################

Door door = new Door(12, 23, "Roble", true);
Console.WriteLine(door.material);
door.Print();

public class Door
{
    public int width;
    public int height;
    public string material;
    public bool open;

    public Door(int width, int height, string material, bool open)
    {
        this.width = width;
        this.height = height;
        this.material = material;
        this.open = open;
    }

    // Implementar un destructor
    ~Door()
    {
        Console.WriteLine("The port it's destruct in this file");
    }

    public void Open()
    {
        open = true; // this is the sintaxis in C#
    }

    public void Close()
    {
        this.open = false;
    }

    public void Print()
    {
        Console.WriteLine($"Width: {this.width} \n Heigth: {this.height} \n Material: {this.material} \n Open: {this.open} \n");
    }
}

// *** Clases en C# ***

// Los 4 pilares fundamentales de la POO
// Encapsulamiento
// Herencia
// Polimorfismo
// Abstraccion

/*
 * Todo objeto tiene 2 partes importantes:
 * Propiedades || Atributos
 * Metodos || Fucniones del objeto
 */

public class Person
{
    private string _name;
    private int _age;
    private string _color;
    private DateTime _bornDate;
    public static string sex = "Male";

    // constructor de la clase Persona
    public Person(string _name, int _age, string _color, DateTime _bornDate) 
    {
        this._name = _name;
        this._age = _age;
        this._color = _color;
        this._bornDate = _bornDate;
    }

    // este es el famosos metodos accesorios a los datos  privados el objeto o clase persona en si
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }

    public int Age
    {
        get
        {
            return _age;
        }

        set
        {
            this._age = value;
        }
    }

    public string Color
    {
        get
        {
            return this._color;
        }
        set
        {
            this._color = value;
        }
    }

    public DateTime BornDate
    {
        get
        {
            return this._bornDate;
        }
        set
        {
            this._bornDate = value;
        }
    }
    public static void SayHello()
    {
        Console.WriteLine("Hello world guys!");
    }
}

// ****** Destructor en C# *********
// Los destructures se utlizar para cuando el GarbageCollector del lenguaje remoueve de la memoria la 
// inicializacion del objeto en si.
// Libera la memoria del computador



// ****************** GENERICS ***************
// Podemos guardar informacion en los genericos de forma sencilla y es mas facil el accesso y trabajo con los datos
// que estan en los genericos.
// Este puede ser un template o una plantilla para crear otroso tipos de datos 
// Esta es la forma de  crear una clase en forma "Generica".


Generic<string> str = new Generic<string>(); // esta es la forma de crearlo
str.Field = "Algun string en la variable";
Console.WriteLine($"Dato: {str.Field}");
Console.WriteLine($"Generic type: {str.Field.GetType().FullName}"); // obtenemos el tipo del dato de la clase. -> String data type.


public class Generic<T>
{
    public T Field { get; set; } // esto es podemos crear un campo  y podemos gettear & settear.
}


//*********** Herencia y Polimorfismo *********************
// La herencia implica compartir la funconalidad o el comportamiento entre clases que jeraquicas
// de padres e hijos ensi.

// Las clases Hijas heredan algunas funcionalidad de sus clases Padres

Coche c = new Coche();
c.gas = 23.245f;
Console.WriteLine(c.gas()); // 23.245f;
c.Stop(); // The coche object is stoped
c.Run() // "Run .. Run .. Run.."
c.rodes = 32;
Console.WriteLine("Coches' rodes: " + c.rodes()); // 32
Console.WriteLine(c.Run("Your coche is mine "));

class Vehicle
{
    public float gas { get; set; }
    public int aforo { get; set; }

    public void Run()
    {
        Console.WriteLine("Run.. Run .. Run");
    }

}

class Coche : Vehicle // con esto le decimo que estamos heredando de la clase Vehicle
{
    public int rodes { get; set; }
    
    // polimorfismo
    public void Run(string message)
    {
        Console.WriteLine("This coche is running" + message+ DateTime.Now.ToShortTimeString()); // nos muestra el formato mas corto de lahora.
    }

    public void Stop()
    {
        Console.WriteLine("The coche object is stoped");
    }
}

// *************** Polimorfismo ****************

// Esta es la representacion de varias formas de un objeto de acuerdo a los valores que se relacionan con este objeto.
// Pueden haber diferentes metodos implementados para el mismo objeto, pueden haber los mismo metodos pero con distintos valores en los argumentos
// que realizar operaciones diferentes en funcion de sus parametros de sus objetos.

// *************** Interfaces *****************
// Estas clases son muy importantes
// Estos son tipos especiales de clases, en el cual no se define la funcionalidad de la clase misma, sino solo se definen 
// los metodos o comportamientos que se deben implementar en otras clases.
// La interfaz es una forma de plantilla que define los lineamientos iniciales sin implmentar, donde las clases que heredan esta interfaz, lo implementan
// los metodos que se definieron en lainterfaz para poder expandirlas
// No  se pueden crear instancias de esta.


interface IVehiculo
{
    void Arrancar();
    void Frenar();
    void Girar(string direccion);
    void Acelerar();
}

class Vehiculo : IVehiculo
{
    void Arrancar()
    {
        Console.WriteLine("Arrancando el coche");
    }

    void Frenar()
    {
        Console.WriteLine("Frenando el coche");
    }

    void Girar(string direccion)
    {
        Console.WriteLine("Girando a: " + direccion);
    }

    void Acelerar()
    {
        Console.WriteLine("Acelerando el coche");
    }
}


// ******************** COLLECTIONS EN C# ***********************
/*
 * Las colecciones son tipos de objetos que se usan para odenar contenido o datos en nuestro proyecto
 * Algunos tipos son mejores para determinadas tareas depende como se utilice.
 * Las Collecions son: 
 * IEnumerable
 * Arrays {index}
 * Listas {index}
 * Diccionarios {key: value}
 * StackList
 * LinkedList
 * HashMap
 * HashSet
 * SortedSet
 * 
 * Las coleciones se usan para administrar objetos de usuarios del mismo tipo AGRUPADOS. Usuarios, numeros en un arreglo
 * con estos datos podemos realizar ciertas operaciones.
 * System.Collections.Generic // sintaxis para definir collections.
 * 
 */

// Collection de Strings
var coches = new List<string>();
coches.Add("Ford");
coches.Add("Ferrari");
coches.Add("BMW");
coches.Add("Supra");

var names = new List<string> { "Roger", "Fiorela", "Yadi", "Margot" };

foreach(var i in coches)
{
    Console.WriteLine($"{i} -> coche -> {i}");
}

foreach(var i in names)
{
    Console.WriteLine($"Names: {i}");
}




Element element = new Element();

List<Element> elementsList = Element.CreateElementList();
List<Element> sorted = elementsList.Sort(); // Esto lo que hace es ordenar la lista. para esto se tiene queimplementar la interfaz "IComparable" propio de C#.


var query = from item in elementsList
            where item._AtomicNumber < 23 || item._Name.Contains('u')
            select item;

foreach(var i in query)
{
    Console.WriteLine("Value: " + i);
}


// print data for elemenst
Element.PrintData();

public class Element
{
    private string _Symbol { set; get; }
    private string _Name { set; get; }
    private string _AtomicNumber { set; get; }

    public static List<Element> CreateElementList()
    {
        List<Element> newList = new List<Element>();
        newList.Add(new Element() { _Symbol: "P", _Name: "Phosforo", _AtomicNumber: "234" });
        newList.Add(new Element() { _Symbol: "S", _Name: "Asufre", _AtomicNumber: "34" });
        newList.Add(new Element() { _Symbol: "Au", _Name: "Gold", _AtomicNumber: "2342" });
        return newList;
    }

    public static void PrintData()
    {
        List<Element> values = CreateElementList();
        foreach(var i in values)
        {
            Console.WriteLine("Values: " + i);
        }
    }

}

// Clase de collecciones Personalizada
// Nota: en la cabecera tenemos que usar System.Collections.Generic
public class MyColors: System.Collections.IEnumerable
{

    Colors[] _colors = { new Colors() { _Name = "red" }, new Colors() { _Name = "blue" }, new Colors { _Name = "green" } };

    public System.Collections.IEnumerable GetEnumerator()
    {
        return new MyColorsEnumerator(_colors); // esto es un enumerador custom
    }
}

public class Colors
{
    private String _Name { get; set; };
}

// Tiempo  en C#
var ahora = DateTime.Now; // 14/12/2021
var ahoraShort = ahora.ToShortTimeString();
var fecha = ahora.ToShortDateString();

TimeOnly soloHora = new TimeOnly(15, 0, 50, 600); // 9:47
DateOnly soloFecha = new DateOnly(2021, 8, 29).ToLongDateString(); // viernes, 29 de agosto de 2021
                                                                   // 

//**************** Enumerados en C# (Enums) *********************
// Estos son  tipos de datos que usualmente usamos para definir tipos de datos DEFINIDOS COMO CONSTANTES pero como un objeto.
// Nos permite definir constantes o datos que no varian en forma de objetos.
// Esto es tratar de colocar en un objeto que no (NO SE PUEDE MODIFICAR - QUE NO SE PUEDE CAMBIAR LOS VALORES EN EL )

enum Seasons
{
    WINTER,
    SUMMER,
    SPRING,
}

enum CodeError
{
    NOTFOUND = 404,
    SUCCESS = 200,
    INFO = 301,
    CREATED = 201,
}

// ******************** TUPLAS en C# **************************
// Este es un tipo de datos para agrupar varios tipos de datos en una structura ligera, y sencilla de gestionar y usar.
// Estructura ligera para estrucutras pequenias  y gestion mas rapida de los datos.

(string, double, int, bool) myTuple = ("Roger", 28.3, 25, True);

var intTuple = (1, 3, 4, 5, 8, 9, 10, 11); // definicon de tuplas enteras.

myTuple.Item1 = "Fiorela";
myTuple.Item2 = 2.34; // se puede modificar las propiedades de las tuplas.
Console.WriteLine($"Item1: {myTuple.Item1}, Item2: {myTuple.Item2}, Item3: {myTuple.Item3++}..."); // se pueden realizar operaciones con  los (ITEMS) de la tupla.





