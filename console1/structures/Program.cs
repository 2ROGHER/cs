﻿// See https://aka.ms/new-console-template for more information

using System;
using System.Text; // Con esto lo que hacemos es usar el StringBuilder
using Motos.MotosApp;

Console.WriteLine("Hello, World!");

// Las estructuras de datos son agrupaciones de datos que nos permiten usarlos
// y usar metodos propios de estas estructuras para trabajar con estos
// 1. Arrays o vectores en una sola linea
// 2. Arrays de 2 dimensiones o vectores de 2 dimension 2x2

Coord coord = new Coord(3, 24, 29);
Console.WriteLine(coord.X); // 3
Console.WriteLine(coord.Y); //24
Console.WriteLine(coord.ToString()); // ({3}, {24})
// Nota importante: La definicon de las estructuras de datso, deben estar en la parte mas baja, 
// mientras que la creacion debe ir la parte superior
// Podemos hacer que nuestra estructura sea solo de lectura. Esto es que cuando se crean los datos ya no se pueden modificar los datos
// de esta estructura de datos. Solo son deletura pero ya no se puede editar.

var p1 = coord with { X = 3 };
var p2 = coord with { Y = 99 };
var p3 = coord with { Z = 324 };

// para readonly: se agrega la sentencia "readonly" despues de public y antes de "struct" sentence.
public   struct Coord
{
    public Coord(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public double X { get; set; } // (get;) es de solo letura: (set;) se puede cambiar los valores
    public double Y { get; set; }
    public double Z { get; set; }
    public override string ToString() => $"({X}, {Y}, {Z})";
}

// bucles while
int k = 0;
while(k < 10)
{   // Nota: WriteLine() escribe en lineas diferentes o establece los saltos de linea
    Console.WriteLine(k);
    // Write(); No se hacen los saltos de linea
    // imprime en la consola en una solalinea
    k++;
}

// bucle (for)
for(int i = 0; i<100; i++)
{
    Console.WriteLine(i);
}

var names = new List<string> { "Fiorela", "Roger", "Luciana" };
// foreach sentence (foreach)
foreach(var name in names)
{
    Console.WriteLine($"Hello, your name is: {name}");
}
// bucle (do-while)
int m = 0;
do
{
    Console.WriteLine(m);
    m++;
    if(m==5)
    {
        continue;
    } else if(m==6)
    {
        continue;
    }
}(m < 100);


// sumar los 100 numeros enteros
int sum = 0;
for(int i = 0; i < 100; i++)
{
    sum += i;
}
// Esta es una forma de interpolacion de variables en la consola.
Console.WriteLine($"La suma para nustro ejemplo es: {sum}");

// definicon de arrays en C#
int[] arr = new int[100];

for(int i = 0; i<=100; i++)
{
    arr[i] = i+3;
}

for(int j=0; j<100; j++)
{
    Console.WriteLine("Item: " + j + "value: " + arr[j]);

}

string op = Convert.ToString(Console.WriteLine("Ingresa el la option correta: ")).ToUpper();

// String && arrays en C#
string? str;
string? str2 = null;
string str3 = System.String.Empty;
string str4 = "";
string str5 = "Hola mundo";
Console.WriteLine(str3.GetType().Name());// String
Console.WriteLine(str4.GetType());// System.String
// chars
string[] chars = { '#', "&", "@" };
Console.WriteLine(chars[2]);// "@"
// Operaciones con strings en C#
string msg1 = "Buenos dias";
string msg2 = "Estoy programando en C#";

// Concatenacion
Console.WriteLine(msg1 + msg2); // Buenos dias Estoy programando en C#
string msg4 = msg1;
msg4 += msg2;
Console.WriteLine(msg4); // Buenos dias Estoy programando en C#.


// Literales
// Estoy son insertar caracteres especiales para "EScapes" o saltos de lineas - similares
// "\t" (este es el famoso tabulador)
string columns = "Column1\t Column2\t Column\t";
Console.WriteLine(columns);

// "\r"(Esto nos permite realizar los famosos saltos de filas)
string rows = "contenido1\r contenido2\r contenido3\t contenido4\r";
Console.WriteLine(rows);
// "\'" (escape para simobolos ''), "\\"(barras invertidas), "\v"(tabulador vertical)

// Mensajes multilinea.
string multilineString = @"
    Hola este es un mensaje de multilineString
    Aqui puedes escribir cualquier cosa ""sin concatenar""";

// Interpolacion de cadenas
var person = (name: "Roger", lastName: "Mestanza", age: "34"); // esta es una tupla en C#.
Console.WriteLine($"{person.name}-{person.lastName}-{person.age}");

// Arrays && Substrings
string miStr = "Este es mi cadena";
// substrings
string miStr2 = miStr.Substring(0, 5); // "Este e";
string miStr3 = miStr.Replace("podcast", "mensaje"); // "Este es mi podcast"


// String Nulos && String vacios
string str = "hello";
string nullStr = null;
string empty = String.Empty; // Este es un string pero (vacio)

string? tempStr = str + nullStr; // hello
Console.WriteLine(tempStr); //
bool b = (empty == nullStr); // False

// StringBuilder (Este es un constructor de string-nos ayuda al momento de crear los strings)
StringBuilder strBuilder = new StringBuilder("Hola StringBuilder");
Console.WriteLine(strBuilder[0]); //H
Console.WriteLine(strBuilder); // Hola StringBuilder

// Casting de Cadenas a Numeros si es posible
int i = 0;
string s = "15239423";
bool result = false;
bool res1 = int.TryParse(s, out i); // la salid es un "boo" i en la variable "i" --> "true", i = 15239423
s = "hola";

bool res2 = int.TryParse(s, i); // False, i= 0;


// Arrays 
int[] arrs = new int[3];
arrs[0] = 1;
arrs[1] = 2;
arrs[3] = 34;
foreach(var i in arrs)
{
    Console.WriteLine(i);
}

int[] numbers = { 3, 25, 49, 21, 2 };
Array.Sort(numbers); // metodo para ordenar el array

foreach(var i in numbers)
{
    Console.WriteLine(i);
}

// Arrays 2D
// definition of arrays
int[,] miArray2D = new int[2, 3]; // definition of arrasy 2d
Console.WriteLine(miArray2D.lenght); // 6


// ********************** FUNCIONES EN C# **************************//

// estrucutra de la funciones e invocaciones de estas mismas
// Invocacion de funciones. Este es el segundo paso.
cuadrado(3);

// Definicion de funciones este es el primer paso 
int cuadrado(int n)
{
    return Math.Pow(n, 2);
}

// Funciones ANONIMAS 
// Son funciones qeu no tienen un nombre para identificarlas- son llamadas tambien FUNCIONES LAMBDA
int[] nums = [3, 42, 525, 934, 3];
var squarePow = nums.Select(x => x * x);
int[] ordered = Array.Sort(nums);

// *************** METODOS ****************/

// uso de metodo Arrancar()
MotoClass moto = new MotoClass();
moto.Arrancar();
MotoClass.ArrancarOn();
// Metodos de extension
moto.CantidadGasolina(3.25, 24.243);


// metodo de MotosApp
Motos.MotoApp.Acelerar();

// Metodo para sobrescribir metodos  de clases en C#
int level = moto.CantidadGasolina(29);
Console.WriteLine($"Ahora tenemos el nivel de: {level}");

double levelDouble = moto.CantidadGasolina(3.3);
Console.WriteLine($"Cantidad de gasolina double: {levelDouble}");

// NOta: Inferencia de tipos se logra con 'var' o para ajustar mas aun con la keyword "dynamic"
dynamic dinamycType = moto.CantidadGasolina(34.23);
Console.WriteLine($"Inferencia de tipos en C#: {dinamycType}");
// class moto
class MotoClass
{
    public static void Arrancar()
    {
        Console.WriteLine("Arrancar la moto");
    }

    public void ArrancarOn()
    {
        Console.WriteLine("Arrancamos la moto");
    }

    public int CantidadGasolina(double precio, double cantidad)
    {
        return precio * cantidad;
    }

    public int CantidadGasolina(int l)
    {
        return l * 30; // this returns a integer
    }

    public double CantidadGasolina(double l)
    {
        return l + 20.5; //  this return a double value
    }
}



// ############################ METODOS EN C# ##############################

