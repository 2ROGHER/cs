/*
 * This file define or Specify a TAD IComplex
 * This file define a interface to  works with this TAD
 */

public interface IComplex
{
    // Define interface methods
    IComplex sum(IComplex z);
    IComplex mult(IComplex z);
    double module(IComplex z1, IComplex z2);

    int Real { get; set; };
    int Imaginay { get; set; };

    int getImaginary(IComplex z);
    void setReal(int x);
    void setImaginary(int y);

    void print();

}