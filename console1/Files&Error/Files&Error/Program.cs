﻿using System.Diagnostics.Tracing;
using System.IO;
using System.Net;
// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

// ********************** Trabajando con Archivos en C# *************************
// Este es un parte importante para el manejo de archivos, como de txt, json, csv entre otros mas
// Se pueden realizar muchas operaciones con el manejo de files en C#.
// Es muy importante el formato de los archivos la momento de trabajar con estos, al realizar las conversiones, entre otras mas.

// Lectura y escritura en C#
// Crear un archivo te text simple
// Para esto tenemos que usar System.IO libreria para trabajr.

string txt = ""; // inicializamos
try
{
    // este es un conjunto o un flujo de caracteres para poder codificar lo que lee.
    //
    StreamReader sr = new StreamReader("myFile.txt");
    // Open the file
    var line = "";

    while(line !=  null) 
    {
        line = sr.ReadLine(); // leemos linea a linea, nos vale para leer la primera linea del archivo "myFile.txt"
        Console.WriteLine($"text: " + line);
    }
    // close the file
    sr.Close();
    Console.ReadKey(); // El flujo se termina cuando se presiona una tecla.

} catch(Exception e)
{
    // el error manejado aqui.
    Console.WriteLine("No se pudo leer el archivo " + txt + e.Message);
}

// Ahora craear un archivo
string fileWrited = "";
try
{
    StreamWriter sw = new StreamWriter("myTextWrited.txt", true); // (path, true), con esto le decimos que se va a agregar otra linea mas 
    sw.WriteLine("Hello World!, all going fun!");
    sw.WriteLine("GoodBye! guys");
    sw.Close();
} catch(Exception e)
{
    Console.WriteLine("No se pudo escribir en el archivo" + e.Message);

}

//************************* Logs en C#  trabajadno con JSON type ***********************
// Podemos usar una API para obtener los datos de ella con C#.
HttpWebRequest request = (HttpWebRequest) HttpWebRequest.Create(@"https://cat-fact.herokuapp.com/facts"); // con el "@" escapamos caracteres raros.
HttpWebResponse response = (HttpWebResponse)request.GetResponse();
Stream stream = response.GetResponseStream();
StreamReader reader = new StreamReader(stream);

var json = reader.ReadToEnd();
Console.WriteLine("El archivo de la web del API es: " + json);


// ********************* MATRICES EN C# ****************************
// int [] arr = new int [10] or int [] arr = new int[] { //data}
// int [,] arr2D = new int [4, 4] // con esto definimos un array de 2D y para poder imprimir los valores usamos arreglos multidimensioanles.



// ************************** EVENTOS EN C# ******************************
// Este es un tipo de paradigma de programacion basada en EVENT DRIVEN PROGRAMMING
// Este es una accion que cuando se desencadena una accion (evento) ante ello algunos objetos (subsriptores) actuan en consecuencia al evento emitido por el emisor
// EMISOR - RECEPTOR
// CAsos de uso: 
// Enumerables -> pulsar un boton, mover el cursos, etc.

// La aplicacion mas practica es cunado un usuario hace click en un boton y se desencadena ciertas acciones.
// Es similar a los eventos que ocurren en JS

// Editor (Emisor, Publisher)
/*
 * - Define el cuando se lanza el evento.
 * - Definicion del evento
 * - Delegado
 */


/* 
 * Utlidad de los eventos:
 * Cuando se envian cietos eventos, un receptor puede controlar estas eventos y realizar ciertas accioenes en consecuencia a los evento que emite el "emisor".
 * Se pueden usar en GUI, web botones, correso entre otros mas.
 */


SubscritorCalculadora calculadora = new SubscritorCalculadora(1, 2);
calculadora.ResultadoSuma();
calculadora.ResultadoResta();
calculadora.ResultadoSaludar(); // llamamos el metod del receptor que fue controlado por el emisor durante el lanzamiento del evento.
public class Emisor()
{
    public delegate void Evento(); // este es el evento que se va a disparar = es una funcion | metodo.
    public event Evento E; // Este es el evento que es de tipo "delegate" y es la implementacion del "Evento"

}

public class EditorCalculadora
{
    public delegate void DelegateCalc();
    public event DelegateCalc Event; // este es el evento

    public void  Sumar(int a, int b)
    {
        if(Event != null)
        {
            Event();
            Console.WriteLine($"La suma es {a + b}");
        }
        Console.WriteLine("No tiens una suscription al evento");
    }

    public void Restar(int a, int b)
    {
        if(Event != null)
        {
            Event(); // emitimos el evento para el subscriptor.
        }
        Console.WriteLine("No tienes una suscription al evento");
    }

    // podemos crear mas eventos que el emisor lanza para los subscriptores
    public void Saludar()
    {
        Console.WriteLine("Hola a todos mis amigos, estoy disfrutanto aprender C#");
    }
}

// Suscriptor (Subsriber, Receptor)
/*
 * - Acepata el evento y provee un (eventHandler)
 * - Metodo que sera ejecutado cuando ocurra el evento
 * - 
 */


public class SubscritorCalculadora
{
    EditorCalculadora emisor;
    private readonly int _A;
    private readonly int _B;

    public void EventHandler()
    {
        Console.WriteLine("Este es el handler del evento: ");
    }

    public SubscritorCalculadora (int a, int b)
    {
        emisor = new EditorCalculadora();
        _A = a;
        _B = b;
        emisor.Event += EventHandler; // con esta linea podemos desencadenar los evento para que el receptoro lo pueda manejar
        // Esta es importante para que los subsriptores puedan "subscribirse" y de esa manera el manejador puedan responder a los eventos
        // con ciertas accioens en este caso "sumar y restar".
    }

    public void ResultadoSuma()
    {
        emisor.Sumar(_A, _B); // realizamos la suma.
    }
    public void ResultadoResta()
    {
        emisor.Restar(_A, _B);
    }

    // tenemos que manejar el evento de saludar 
    public void ResultadoSaludar()
    {
        emisor.Saludar(); // manejamos elevento y realizamos la accion,.
    }
}
// Evento 
/*
 * Este es el objeto que transmite las acciones que define el Emisor
 * Se realiza mediante metodos que se desencadena cuando se cumple una condicion
 */