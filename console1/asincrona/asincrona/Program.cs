﻿// See https://aka.ms/new-console-template for more information
using System.Diagnostics;

Console.WriteLine("Hello, World!");

// ************ Programacion Asincrona en C# *********************
/*
 * La programacion nos permite realizar varias tareas o varios procesos en paralelo
 * C# tiene la propiedad de ser multithreading varios hilos.
 * Con esta cualidad podemos hacer varias tareas en paralelo (al mismo tiempo)
 * Java no es multithreading.
 * Podemos gestionar varias tareas que se realizan en paralelo, con ello podemos aumentar el rendimiento, dado que
 * podemos hacer varias tareas en una sola.
 * Cada hilo es como una persona individual realizando o ordenadores realizando procesos en paralelo o al mismo tiempo.
 * Nosotros podemos usar threading y Tasks por medio de "Diagnostics, Threading, Tasks" los cuales son procesos para realizar en paralelo.
 * 
 */

Stopwatch crono = new Stopwatch(); // tenemos un objeto crono de tipo Stopwatch.
crono.Start(); // tenemos que iniciaarlo
// Logica que nos tome tiempo hacerlo
int sum = 0;
for(int i=0; i < 100; i ++)
{
    // Logica compleja que se va a realizar en paralelo.
    sum += i;
}

// Tareas en paralelo.

// 1. primero creamos la tares
Stopwatch sw = Stopwatch.StartNew();
// Task1
var task1 = new Task(() => {
    Stopwatch crono = new Stopwatch();
    crono.Start();
    Thread.Sleep(1000);
    crono.Stop();
    Console.WriteLine("Task1: " + crono.Elapsed);
}); // las tareas reciben funciones anomimas como Callbacks para la realizacion de tareas.


// Task2
var task2 = new Task(() => {
    Stopwatch crono = new Stopwatch();
    crono.Start();
    Thread.Sleep(2000);
    crono.Stop();
    Console.WriteLine("Task2: " + crono.Elapsed);
}); // las tareas reciben funciones anomimas como Callbacks para la realizacion de tareas.



// Task3
var task3 = new Task(() => {
    Stopwatch crono = new Stopwatch();
    crono.Start();
    Thread.Sleep(1600);
    crono.Stop();
    Console.WriteLine("Task3: " + crono.Elapsed);
}); // las tareas reciben funciones anomimas como Callbacks para la realizacion de tareas.


// Ahora lanzamos las tareas en paralelo para su ejecucion
// Con esto lo que hacemos es lanzar las tareas en paralelo o en segundo plano.
task1.Start();
task2.Start();
task3.Start();

// Ahora tenmos que esperar las tareas asincronas
await task1;
await task2;
await task3;

// ahora detenemos el proceso asyncrono
sw.Stop();
Console.WriteLine($"Todo Main: {sw.Elapsed}s duro en total ");

Thread.Sleep(1000); // Con esto le decimo que el hilo se duerma o demore un segundo.



Console.WriteLine(crono.Elapsed); // Medimos el tiempo que nos tomo hacer el proceso en paralelo.

// Task 4
var res = await RandomAsync(); // esta es la sintaxis para gestionar tareas asyncronas.
Console.WriteLine(res);

crono.Stop(); // lo detenemos la ejecucion de la tarea.
static async Task<string> RandomAsync()
{
    Stopwatch sw = Stopwatch.StartNew(); // Esto  por default ya lo inicia el watcher.
    var num = new Random().Next(1000); // mil numeros random;
    Thread.Sleep(1000); // Esto nos dice que tenmos 4 procesos  y que cada tarea tarda 1 segundo.
    var res = num.ToString() + "calculando en: " + sw.Elapsed;
    sw.Stop();
    Console.WriteLine();
    return res;
}


// *************** MULTITHREADING EN C# *********************
/*
 * Esta es una caracteristica para gestionar diversos FLUJOS trabajo.
 * Un hilo es basicamente un proceso (hilo) que realiza ciertos procesos durante el flujo de trabajo.
 * Esta peculiaridad nos permite realizar varias tareas en paralelo.
 * Cada hilo es un proceso que se realiza, es un proceso que se realiza para obtener y extraer o en mayor de los casos
 * realizar operaciones para los que se ha escrito el programas
 *  Main thread -> Pedimos datos de la db.
 *  Hilo 1 -> conexcion a la BBDD -> LINQ -> hacer peticion
 *  Hilo 2 -> consultas a una API
 *  Hilo 3 -> etc.
 *  Estas tareas no interrumpen el flujo del Hilo Main o principal.
 *  * Para esto se tiene que ver los recursos y los flujos de trabajo.
 */


// Sin multithreading
Stopwatch stw = Stopwatch.StartNew(); // inicializamos una para medir una tarea.
stw.Start(); // inicializamos
// task 1
// Le decimo que el hilo principal demore 2s;
Thread.Sleep(2000);


// task 2
// Le decimo que el hilo principal demore 2s;
Thread.Sleep(2000);

// task 3
// Le decimo que el hilo principal demore 2s;
Thread.Sleep(2000);

stw.Stop(); // finalizamos
// Aqui el enfoque es que nosotros estamos bloqueados mientras se estan realizando cada una de estas tareas (NO ES MULTIHILO).
Console.WriteLine($"Este programa ha tardao: " + stw.Elapsed); // en total estas tareas van a demorar aprox. 6s en total.



// Main Thread o Hilo Principal
Thread hiloPrincipal = Thread.CurrentThread;
hiloPrincipal.Name = "Hilo Principal";

// Multithreading
Stopwatch sw1 = new Stopwatch();
sw1.Start();
ThreadStart refHilo = new ThreadStart(CreacionHilos.IniciarHilos); // Con esto le decimos que inicie un hilo con una referencia
Thread hiloSecundario = new Thread(refHilo);
hiloSecundario.Start(); // iniciamos la creacion de hilo secundario.
sw1.Stop();
Console.WriteLine("El hilo a demorado en su ejecucion: " + sw1.Elapsed);
// Creacion de hilos
class CreacionHilos
{
    public static void IniciarHilos()
    {
        Console.WriteLine("Iniciando Hilo nuevo");
        Thread.Sleep(1000);
        Console.WriteLine("Terminando las tareas");
    }
}

// Esto corresponde a la parte de  Multithread
// Un concepto muy importante son las TAREAS, estas se ejecuta en hilos y ASINCRONAMENTE 
// Task1

Stopwatch watchGlobal = new Stopwatch();
watchGlobal.Start();
var todo1 = new Task(() =>
{
    var watch = Stopwatch.StartNew();
    Thread.Sleep(1000); // paramos elhilo 1 seguno
    watch.Stop();
    Console.WriteLine($"El hilo ha tardado: {watch.Elapsed}");
});

// Task2 
var todo2 = new Task(() =>
{
    var watch = Stopwatch.StartNew();
    Thread.Sleep(2000); // paramos el hilo durante 2s
    watch.Stop();
    Console.WriteLine($"El hilo  todo2 ha tardado: {watch.Elapsed}");
});

// Se ha definido todas las tareas pero ahora no estan haciedno nada.
// Para poder disparar el inicio de las tareas tenemos que INICIALIZARLOS.
todo1.Start();
todo2.Start();

// Ahora como hacemos para recibir las tareas, tenemos que usar las "awaits" para poder gestionar estas
// Esta es la forma de hacerlo individualmete.
await todo1;
await todo2;

// O podemos hacer de la siguiente manera para poder recibir las tareas que hemos creado.
Task.WhenAll(todo1, todo2); // con esto gestionamos todas la tareas de golpe.
watchGlobal.Stop();
Console.WriteLine($"La tareas han demorado un tiempo de: {watchGlobal.Elapsed}");

